package com.example.dave.drinkable;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.HashMap;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;


public class GraphActivity extends AppCompatActivity {
    BluetoothSPP bluetooth;
    ProgressDialog progressDialog;
    String s2;
    TextView prova;
    int Z;
    private LineGraphSeries<DataPoint> mSeries1;
    private LineGraphSeries<DataPoint> mSeries2;
    ArrayList<Integer> arraylist = new ArrayList<>();
    GraphView graph;
    int j =0;
    Double TBW;
    String H;
    String W;
    String A;
    String Sesso;
    int S;
    String id;
    DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bluetooth = new BluetoothSPP(this);
        progressDialog = new ProgressDialog(GraphActivity.this);
        final Button button = findViewById(R.id.ble);
        graph = findViewById(R.id.graph);
        prova = findViewById(R.id.prova);
        id = FirebaseAuth.getInstance().getCurrentUser().getUid();
        ref = FirebaseDatabase.getInstance().getReference().child("Utenti").child(id);
        prova.setText("Non connesso");
        final HashMap<String,Object> hashMap = new HashMap<>();
        button.setVisibility(View.VISIBLE);

        A = getSharedPreferences("userData", 0).getString("eta","eta");
        H = getSharedPreferences("userData", 0).getString("altezza", "altezza");
        W = getSharedPreferences("userData", 0).getString("peso", "peso");
        Sesso = getSharedPreferences("userData", 0).getString("sesso", "sesso");

        if (Sesso.equals("Male")){
            S = 1;

        }else{

            S = 0;
        }

        Log.d("DATI_SHARE", " " + H + " " + A + " " + W + " " + S + " ");


        if (!bluetooth.isBluetoothAvailable()) {
            Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_SHORT).show();
            finish();
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bluetooth.getServiceState() == BluetoothState.STATE_CONNECTED) {
                    bluetooth.disconnect();
                } else {
                    Intent intent = new Intent(getApplicationContext(), DeviceList.class);
                    startActivityForResult(intent, BluetoothState.REQUEST_CONNECT_DEVICE);
                    progressDialog.setMessage("Wait for the connection");
                    progressDialog.show();
                }

            }
        });

        bluetooth.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            public void onDeviceConnected(String name, String address) {
                Toast.makeText(getApplicationContext(), "Device connected", Toast.LENGTH_SHORT).show();
                prova.setText("Connesso a " + name);
                prova.setTextColor(Color.BLUE);
                progressDialog.dismiss();
                button.setVisibility(View.INVISIBLE);

            }

            public void onDeviceDisconnected() {
                Toast.makeText(getApplicationContext(), "Connection lost", Toast.LENGTH_SHORT).show();
                prova.setText("Non connesso");
                button.setVisibility(View.VISIBLE);
            }

            public void onDeviceConnectionFailed() {
                Toast.makeText(getApplicationContext(), "Unable to connect", Toast.LENGTH_SHORT).show();
                prova.setText("Non connesso");
                button.setVisibility(View.VISIBLE);
            }
        });


        bluetooth.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {
                Z = new Integer(new String(data))/10;
                hashMap.put("values", Z);
                ref.updateChildren(hashMap);
                Log.d("DATA", " " + Z);
                Log.d("MESSAGE", " " + message);
                Log.d("TBW", " " + TBW);

            }
        });

        mSeries1 = new LineGraphSeries<>();
        mSeries2 = new LineGraphSeries<>();
        mSeries2.setColor(Color.RED);

        graph.addSeries(mSeries1);
        graph.addSeries(mSeries2);

        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(300);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(400);


        }

    public void GenerateSeries2(){
        mSeries2.appendData(new DataPoint(j++, 50), true, 5000);
    }


    int i = 0;
    int a;
    Integer sum = 0;
    Double average;
    public void  generateData() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                a = i++;
            }
        }, 5000);
        if(Z != 20260) {
            if(Z > 0 && Z < 1000){
                arraylist.add(Z);
                if(!arraylist.isEmpty()){
                    for (Integer mark : arraylist) {
                        sum += mark;
                        average = sum.doubleValue() / arraylist.size();
                        //GenerateSeries2(average);
                    }
                }else {

                    Toast.makeText(getApplicationContext(), "Vuoto", Toast.LENGTH_SHORT).show();
                }

            }
            mSeries1.appendData(new DataPoint(a, Z), true, 5000);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new Thread(new Runnable() {

            @Override
            public void run() {
                // we add 100 new entries
                for (int i = 0; i < 1000; i++) {
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            generateData();
                            GenerateSeries2();
                        }
                    });

                    // sleep to slow down the add of entries
                    try {
                        Thread.sleep(600);
                    } catch (InterruptedException e) {
                        // manage error ...
                    }
                }
            }
        }).start();
    }



    public void onStart() {
        super.onStart();
        if (!bluetooth.isBluetoothEnabled()) {
            bluetooth.enable();
        } else {
            if (!bluetooth.isServiceAvailable()) {
                bluetooth.setupService();
                bluetooth.startService(BluetoothState.DEVICE_OTHER);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        bluetooth.stopService();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BluetoothState.REQUEST_CONNECT_DEVICE) {
            if (resultCode == Activity.RESULT_OK)
                bluetooth.connect(data);
        } else if (requestCode == BluetoothState.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {
                bluetooth.setupService();
            } else {
                Toast.makeText(getApplicationContext()
                        , "Bluetooth was not enabled."
                        , Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

}
