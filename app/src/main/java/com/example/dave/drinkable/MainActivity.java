package com.example.dave.drinkable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.HashMap;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import app.akexorcist.bluetotohspp.library.DeviceList;

public class MainActivity extends AppCompatActivity {
    BluetoothSPP bluetooth;
    ProgressDialog progressDialog;
    static public String s2;
    int TBW;
    int H;
    String eta_data;
    String peso_data;
    String altezza_data;
    String sesso;
    int selectedId;
    SharedPreferences playerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        Button button = findViewById(R.id.ble);
        bluetooth = new BluetoothSPP(this);
        progressDialog = new ProgressDialog(MainActivity.this);
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final EditText eta = findViewById(R.id.eta);
        final EditText peso = findViewById(R.id.peso);
        final EditText altezza = findViewById(R.id.altezza);
        final RadioGroup radioGroup = findViewById(R.id.radioGrp);
        final HashMap<String, Object> hash = new HashMap<>();
        SharedPreferences sharedPrefs = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor ed;
        if (sharedPrefs.contains("eta")) {
            GoToMain();
        } else {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    eta_data = eta.getText().toString();
                    peso_data = peso.getText().toString();
                    altezza_data = altezza.getText().toString();
                    selectedId = radioGroup.getCheckedRadioButtonId();
                    RadioButton radioM = findViewById(selectedId);
                    sesso = radioM.getText().toString();
                    if (!(eta_data.equals("") || peso_data.equals("") || altezza_data.equals("") || selectedId == 0)) {
                        String id = FirebaseAuth.getInstance().getCurrentUser().getUid();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Utenti").child(id);
                        hash.put("età", eta_data);
                        hash.put("peso", peso_data);
                        hash.put("altezza", altezza_data);
                        hash.put("sesso", sesso);
                        ref.updateChildren(hash);
                        SharedPref();
                    } else {

                        Toast.makeText(getApplicationContext(), "Riempire tutti i campi", Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }

    public void SharedPref(){

        getSharedPreferences("userData",0 ).edit().putString("eta",eta_data).apply();
        getSharedPreferences("userData",0 ).edit().putString("peso",peso_data).apply();
        getSharedPreferences("userData",0 ).edit().putString("altezza",altezza_data).apply();
        getSharedPreferences("userData",0 ).edit().putString("sesso",sesso).apply();
        GoToGraph();
    }

    public void GoToGraph(){

        Intent intent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(intent);
        MainActivity.this.finish();

    }

    public void GoToMain(){
        Intent intent = new Intent(getApplicationContext(), GraphActivity.class);
        startActivity(intent);
        MainActivity.this.finish();

    }

}
