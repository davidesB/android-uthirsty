package com.example.dave.drinkable;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class AutenticationActivity extends AppCompatActivity {
    EditText nome;
    SharedPreferences playerData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autentication);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        nome = findViewById(R.id.nome_edit);
        final EditText email = findViewById(R.id.email_edit);
        final EditText password = findViewById(R.id.pwd_edit);
        Button registrazione = findViewById(R.id.registazione_button);
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();
        final HashMap<String, Object> sens_t = new HashMap<>();
        SharedPreferences sharedPrefs = getSharedPreferences("username", MODE_PRIVATE);
        SharedPreferences.Editor ed;
        if (sharedPrefs.contains("username")) {
            GoToMain();
        } else {
            registrazione.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!(email.getText().toString().equals("") || password.getText().toString().equals(""))) {
                        mAuth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(getApplicationContext(), "Utente Registrato", Toast.LENGTH_SHORT).show();
                                SharedPref();
                                String id = FirebaseAuth.getInstance().getCurrentUser().getUid(); //prende l'id dell'utente e la mette nella variabile id
                                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Utenti").child(id);
                                sens_t.put("values", "0");
                                ref.updateChildren(sens_t);
                                GoToMain();


                            }
                        });
                    }
                }
            });
        }
    }

    public void SharedPref(){

        getSharedPreferences("username",0 ).edit().putString("username",nome.getText().toString()).apply();

    }

    public void GoToMain(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        AutenticationActivity.this.finish();

    }
}
